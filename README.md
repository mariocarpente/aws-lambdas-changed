# AWS Lambdas Changed
A python script is stored in this repository that allows detecting if the local code of a lambda has changed, comparing it with the current deployed version. The result of the comparison is returned in a text file with the format ```${fuctionName};${path}```.

The script has been packaged in a docker image so that it can be used as an image in a CI/CD pipeline. The [image](https://hub.docker.com/repository/docker/marcarvar/aws-lambdas-changed) is uploaded to DockerHub.

## Execution options
The available options and arguments are shown below:

```sh
$ python3 src/lambdas-changed.py --help 
Usage: lambdas-changed.py [OPTIONS] PATHS...

  Compare local code with deploy code in lambda

Arguments:
  PATHS...  paths to the files or folders that make up the code for the
            lambdas. The name of each is used to search the lambda  [required]

Options:
  --prefix TEXT                   Prefix added to the name of the file or
                                  directory to form the name of the lambda
                                  function.
  --suffix TEXT                   Suffix added to the name of the file or
                                  directory to form the name of the lambda
                                  function.
  --output-path TEXT              Path to save the file with the name of the
                                  changed lambdas.  [default: ./lambdas-
                                  changed.txt]
  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.
  --help                          Show this message and exit.
```