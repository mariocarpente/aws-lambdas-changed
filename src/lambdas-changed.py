#!/usr/local/bin/python3
import  wget, os, zipfile
import shutil
import boto3, botocore

import hashlib
from checksumdir import dirhash

import typer
from pathlib import Path
from typing import List

TEMP_DIR = "./temp/"


def get_file_md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def is_lambda_code_changed(local_path, download_path):
    if os.path.isfile(local_path):
        files_download = os.listdir(download_path)
        if len(files_download) > 1:
            return True
        return get_file_md5(local_path) != get_file_md5(download_path+"/"+files_download[0])
    # If is dir use dirhash
    return dirhash(local_path, 'md5') != dirhash(download_path, 'md5')


def download_lambda_code(client, function_name):
    try:
        response = client.get_function(FunctionName=function_name)
    except botocore.exceptions.ClientError as error:
        shutil.rmtree(TEMP_DIR)
        if error.response['Error']['Code'] == 'ResourceNotFoundException':
            raise Exception("Lambda fuction with name "+function_name+" not exist in region "+ client.meta.region_name)
        else:
            raise error
    # Get zip url with code of function
    zip_url = response['Code']['Location']
    # Download and unzip
    filename = wget.download(zip_url)
    with zipfile.ZipFile(filename, 'r') as zip_ref:
        zip_ref.extractall(TEMP_DIR + function_name)
    os.remove(filename)
    return TEMP_DIR + function_name


def get_path_name(path):
    path = str(path)
    if os.path.exists(path):
        if os.path.isfile(path):
            return os.path.splitext(path)[0]
        if os.path.isdir(path):
            return path.split("/")[-1]
    else:
        shutil.rmtree(TEMP_DIR)
        raise Exception("Path "+path+" not exist")

def create_lambda_client():
    # Get and check AWS_ACCESS_KEY_ID enviroment variable
    aws_access_key = os.environ.get('AWS_ACCESS_KEY_ID')
    if aws_access_key is None:
        raise Exception("Enviroment variable AWS_ACCESS_KEY_ID not found")
    # Get and check AWS_SECRET_ACCESS_KEY enviroment variable
    aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
    if aws_secret_access_key is None:
        raise Exception("Enviroment variable AWS_SECRET_ACCESS_KEY not found")
    # Get AWS_SESSION_TOKEN enviroment variable
    aws_session_token = os.environ.get('AWS_SESSION_TOKEN')
    # Get AWS_REGION and AWS_DEFAULT_REGION enviroment variable
    aws_region = os.environ.get('AWS_REGION')
    aws_default_region = os.environ.get('AWS_DEFAULT_REGION')
    if aws_region is None and aws_default_region is None:
        raise Exception("Enviroment variable AWS_REGION or AWS_DEFAULT_REGION not found")
    # If AWS_REGION is not define use AWS_DEFAULT_REGION
    if aws_region is None:
        aws_region = aws_default_region
    # Create lambda client with credentials
    return boto3.client(
            'lambda',
            aws_access_key_id= aws_access_key,
            aws_secret_access_key= aws_secret_access_key,
            aws_session_token= aws_session_token,
            region_name= aws_region)


def main(
    paths: List[Path] = typer.Argument(..., help="paths to the files or folders that make up the code for the lambdas. The name of each is used to search the lambda"),
    prefix: str = typer.Option("", help="Prefix added to the name of the file or directory to form the name of the lambda function."),
    suffix: str = typer.Option("", help="Suffix added to the name of the file or directory to form the name of the lambda function."),
    output_path: str = typer.Option("./lambdas-changed.txt", help="Path to save the file with the name of the changed lambdas."),
):
    """
    Compare local code with deploy code in lambda
    """
    
    # Create lambda client SDK with AWS credentials
    client = create_lambda_client()
    
    countLambdasChanged = 0
    # Write to the output file the name and path of the changed lambdas
    with open(output_path, 'w', encoding='utf-8') as f:
        for path in paths:
            function_name = prefix + get_path_name(path) + suffix
            download_path = download_lambda_code(client, function_name)
            if is_lambda_code_changed(path, download_path):
                f.write(function_name+";"+str(path)+"\n")
                countLambdasChanged+=1
    print("\nThe number of lambdas changed is " + str(countLambdasChanged) + " out of " + str(len(paths)))
    shutil.rmtree(TEMP_DIR)


if __name__ == "__main__":
    typer.run(main)

